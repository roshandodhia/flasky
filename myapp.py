# imports
from flask import Flask, jsonify, request


app = Flask(__name__)


# home endpoint
@app.route('/')
def home():
    return jsonify(data='hello')

if __name__ == '__main__':
    # basically, saying if this is being imported the app won't run
    # conversely, only run this app if this script is being run
    app.run(host='0.0.0.0', port=5000)


